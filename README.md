# runInX.sh
Runs application at different vty.

Notes:
* run from terminal (!), if running from shortcut, tick "run in terminal" checkmark (!)
* you will probably need to have /etc/X11/xorg.conf generated (ex.: # nvidia-xconfig) if it does not exist yet

About: creates isolated speedy environment for safely running fullscreen applications.

Use cases:
* with fullscreen applications that run into problems when Alt+Tabbed away from (ex.: some Wine apps)
* applications that can crash or exit improperly - instead of freezing the whole desktop, only one "isolated" virtual terminal suffers (recoverably) (ex.: some Windows games or more serious software)

Arguments order: runInX.sh <executable> <maximised> <resolution> <wm> <workpath> <terminal [2-6]> <commands before start>  <commands after end>

Compulsory requirements:
* xinit (xorg-server, xorg-server-common, xorg-server-devel, xorg-xinit)

Optional requirements (under some circumstances none may be needed):
* steam - needed if application exe is not defined (big picture Steam client is launched)
* bspwm - chosen lightweight window manager
* sxhkd - dependency for bspwm's window management
* unclutter - hide annoying idle cursor

Usage experience:
* With runWithX.sh script I get MUCH higher framerate than if running from my KDE Plasma desktop as usually.
* Some apps are slow to unload from memory so black screen couple of seconds long is to be expected before automatic switch to desktop happens (we can just return to desktop manually and even kill process with Ctrl+C afterwards).
* Big picture Steam mode (default if no executable is specified) can only (?) be stopped manually from dekstop.

TODO:
* Finish help function...
* Apply more tests and fix possible bugs.
* Find optimal solutions for everything.
* Get rid of requirements / replace with more lightweight alternatives.

Feel free to use it, advise me of changes. Have fun!

Example:
runInX.sh -r '1366x768' -s -w '/home/user/Games/Lego\ Star\ Wars' -t 3 -b '/home/user/Games/Lego\ Star\ Wars/bin/mountCd.sh' -a '/home/user/Games/Lego\ Star\ Wars/bin/unmountCd.sh' '/home/user/Games/Lego\ Star\ Wars/bin/start.sh'
